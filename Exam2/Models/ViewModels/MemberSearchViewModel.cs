﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Exam2.DBModel;
using MemberList = Exam2.DBModel.MemberList;

namespace Exam2.Models.ViewModels;

public class MemberSearchViewModel
{
    public string NameCondition { get; set; }
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy}")]
    public string BirthdayYearCondition { get; set; }
    public List<Member> MemberLists { get; set; }
}