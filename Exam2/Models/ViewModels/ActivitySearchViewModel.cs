﻿using System.ComponentModel.DataAnnotations;
using Exam2.DBModel;

namespace Exam2.Models.ViewModels;

public class ActivitySearchViewModel
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Date { get; set; }
    public int Fee { get; set; }
    public int ParticipantNumber { get; set; }
    public string ActivityNameCondition { get; set; }
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public string StartDateCondition { get; set; }
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public string EndDateCondition { get; set; }
    public List<ActivityList> ActivityLists { get; set; }
}