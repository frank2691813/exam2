﻿using System.ComponentModel.DataAnnotations;

namespace Exam2.Models.ViewModels;

public class MemberDetailViewModel
{
    public int Id { get; set; }
    [Required(ErrorMessage = "請輸入姓名")]
    public string Name { get; set; }
    [Required(ErrorMessage = "請輸入性別")]
    public string Sex { get; set; }
    [Required(ErrorMessage = "請輸入生日")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public string Birthday { get; set; }
    [Required(ErrorMessage = "請輸入身分證字號")]
    [RegularExpression(@"^[a-zA-Z]\d{9}", ErrorMessage = "身分證字號格式錯誤，請重新輸入")]
    public string IdentityNumber { get; set; }
    [EmailAddress(ErrorMessage = "信箱格式錯誤，請重新輸入"), Required(ErrorMessage = "請輸入Email")]
    public string Email { get; set; }
    [Required(ErrorMessage = "請輸入手機號碼")]
    [RegularExpression(@"09\d{8}", ErrorMessage = "手機號碼格式錯誤，請重新輸入")]
    public string PhoneNumber { get; set; }
    [Required(ErrorMessage = "請輸入通訊地址")]
    public string Address { get; set; }
    [Required(ErrorMessage = "請輸入學校")]
    public string School { get; set; }
    [Required(ErrorMessage = "請輸入科系")]
    public string Department { get; set; }
}