﻿using System.ComponentModel.DataAnnotations;

namespace Exam2.Models.ViewModels;

public class ActivityDetailViewModel
{
    public int Id { get; set; }
    [Required(ErrorMessage = "請輸入活動名稱")]
    public string Name { get; set; }
    [Required(ErrorMessage = "請輸入日期(起)")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public string StartDate { get; set; }
    [Required(ErrorMessage = "請輸入日期(訖)")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
    public string EndDate { get; set; }
    [Required(ErrorMessage = "請輸入地點")]
    public string Place { get; set; }
    [Required(ErrorMessage = "請輸入費用")]
    public int Fee { get; set; }

    public List<ParticipantBox> SelectCheckBoxes { get; set; }
    
    public List<ParticipantBox> UnSelectCheckBoxes { get; set; }
}

public class ParticipantBox
{
    public int MemberId { get; set; }
    public string MemberName { get; set; }
}