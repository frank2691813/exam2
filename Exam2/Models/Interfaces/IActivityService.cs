﻿using Exam2.Models.ViewModels;

namespace Exam2.Models.Interfaces;

public interface IActivityService
{
    Task<ActivitySearchViewModel> ActivityListQueryAsync(ActivitySearchViewModel activitySearchViewModel);
    Task<ActivityDetailViewModel> ActivityListQueryByIdAsync(string id);
    Task<bool> ActivityAddAsync(ActivityDetailViewModel activityDetailViewModel);
    Task<bool> ActivityEditAsync(ActivityDetailViewModel activityDetailViewModel);
}