﻿using Exam2.DBModel;
using Exam2.Models.ViewModels;

namespace Exam2.Models.Interfaces;

public interface IMemberService
{
    Task<MemberSearchViewModel> MemberListQuery(MemberSearchViewModel memberSearchViewModel);
    Task<MemberDetailViewModel> MemberQueryById(string id);
    Task<(bool, string)> MemberAddAsync(MemberDetailViewModel memberDetailViewModel);
    Task<(bool, string)> MemberEditAsync(MemberDetailViewModel memberDetailViewModel);
    Task<List<ParticipantBox>> GetAllMemberList();
}