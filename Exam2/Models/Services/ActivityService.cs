﻿using AutoMapper;
using Exam2.DBModel;
using Exam2.Models.Interfaces;
using Exam2.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Exam2.Models.Services;

public class ActivityService : IActivityService
{
    private readonly Exam2Context _context;
    private readonly IMapper _mapper;

    public ActivityService(Exam2Context context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<ActivitySearchViewModel> ActivityListQueryAsync(ActivitySearchViewModel activitySearchViewModel)
    {
        var activities = _context.ActivityLists.AsQueryable().AsNoTracking();
        if (!string.IsNullOrWhiteSpace(activitySearchViewModel.ActivityNameCondition))
        {
            activities = activities.Where(d => d.Name == activitySearchViewModel.ActivityNameCondition);
        }

        if (!string.IsNullOrWhiteSpace(activitySearchViewModel.StartDateCondition))
        {
            activities = activities.Where(d => d.StartDate >= DateTime.Parse(activitySearchViewModel.StartDateCondition));
        }

        if (!string.IsNullOrWhiteSpace(activitySearchViewModel.EndDateCondition))
        {
            activities = activities.Where(d => d.EndDate <= DateTime.Parse(activitySearchViewModel.EndDateCondition));
        }

        if (activities.Any())
            activitySearchViewModel.ActivityLists = await activities.ToListAsync();

        return activitySearchViewModel;
    }

    public async Task<ActivityDetailViewModel> ActivityListQueryByIdAsync(string id)
    {
        ActivityDetailViewModel activityDetailViewModel;
        try
        {
            var activityId = int.TryParse(id, out var num) ? int.Parse(id) : num;
            var activity = await _context.ActivityLists.AsQueryable().AsNoTracking().FirstOrDefaultAsync(d => d.Id == activityId);
            var allMemberParticipantBoxes = _context.Members.AsQueryable().AsNoTracking()
                .Select(d => new ParticipantBox {MemberId = d.Id, MemberName = d.Name});
            if (activity is null)
                return new ActivityDetailViewModel();

            var selectMemberParticipantBoxes = from selectParticipants in _context.ActivityParticipants.AsQueryable()
                    .AsNoTracking()
                join allMember in allMemberParticipantBoxes on selectParticipants.MemberId equals allMember.MemberId
                where selectParticipants.ActivityId == activityId
                select new ParticipantBox
                {
                    MemberId = selectParticipants.MemberId,
                    MemberName = allMember.MemberName
                };

            var unSelectMemberParticipantBoxes = allMemberParticipantBoxes.Except(selectMemberParticipantBoxes);

            activityDetailViewModel = _mapper.Map<ActivityDetailViewModel>(activity);
            activityDetailViewModel.StartDate = activity.StartDate.ToString("yyyy-MM-dd");
            activityDetailViewModel.EndDate = activity.EndDate.ToString("yyyy-MM-dd");
            activityDetailViewModel.SelectCheckBoxes = await selectMemberParticipantBoxes.ToListAsync();
            activityDetailViewModel.UnSelectCheckBoxes = await unSelectMemberParticipantBoxes.ToListAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return activityDetailViewModel;
    }

    public async Task<bool> ActivityAddAsync(ActivityDetailViewModel activityDetailViewModel)
    {
        try
        {
            var activity = _mapper.Map<ActivityList>(activityDetailViewModel);
            activity.StartDate = DateTime.Parse(activityDetailViewModel.StartDate);
            activity.EndDate = DateTime.Parse(activityDetailViewModel.EndDate);
            activity.ParticipantNumber = activityDetailViewModel.SelectCheckBoxes?.Count ?? 0;

            _context.ActivityLists.Add(activity);
            await _context.SaveChangesAsync();
            if (activityDetailViewModel.SelectCheckBoxes is not null && activityDetailViewModel.SelectCheckBoxes.Any())
            {
                var activityParticipants = activityDetailViewModel.SelectCheckBoxes.Select(d => new ActivityParticipant
                    {MemberId = d.MemberId, ActivityId = activity.Id});
                await _context.ActivityParticipants.AddRangeAsync(activityParticipants);
                await _context.SaveChangesAsync();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
        
        return true;
    }

    public async Task<bool> ActivityEditAsync(ActivityDetailViewModel activityDetailViewModel)
    {
        try
        {
            var activity = _mapper.Map<ActivityList>(activityDetailViewModel);
            activity.StartDate = DateTime.Parse(activityDetailViewModel.StartDate);
            activity.EndDate = DateTime.Parse(activityDetailViewModel.EndDate);
            activity.ParticipantNumber = activityDetailViewModel.SelectCheckBoxes?.Count ?? 0;

            _context.ActivityLists.Update(activity);
            _context.ActivityParticipants.RemoveRange(_context.ActivityParticipants.Where(d => d.ActivityId == activity.Id));
            await _context.SaveChangesAsync();

            if (activityDetailViewModel.SelectCheckBoxes is not null && activityDetailViewModel.SelectCheckBoxes.Any())
            {
                await _context.ActivityParticipants.AddRangeAsync(activityDetailViewModel.SelectCheckBoxes.Select(d =>
                    new ActivityParticipant {MemberId = d.MemberId, ActivityId = activity.Id}));
                await _context.SaveChangesAsync();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return true;
    }
}