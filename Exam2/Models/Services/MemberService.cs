﻿using System.Linq.Expressions;
using AutoMapper;
using Exam2.Controllers;
using Exam2.DBModel;
using Exam2.Models.Interfaces;
using Exam2.Models.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Exam2.Models.Services;

public class MemberService : IMemberService
{
    private readonly Exam2Context _context;
    private readonly IMapper _mapper;

    public MemberService(Exam2Context context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    public async Task<MemberSearchViewModel> MemberListQuery(MemberSearchViewModel memberSearchViewModel)
    {
        var members = _context.Members.AsQueryable();
        if (!string.IsNullOrWhiteSpace(memberSearchViewModel.NameCondition))
        {
            members = members.Where(d => d.Name == memberSearchViewModel.NameCondition);
        }

        if (!string.IsNullOrWhiteSpace(memberSearchViewModel.BirthdayYearCondition))
        {
            members = members.Where(d => d.Birthday.Year == DateTime.Parse($"{memberSearchViewModel.BirthdayYearCondition}-01-01").Year);
        }

        if (members.Any())
            memberSearchViewModel.MemberLists = await members.ToListAsync();

        return memberSearchViewModel;
    }

    public async Task<MemberDetailViewModel> MemberQueryById(string id)
    {
        var memberId = int.TryParse(id, out var num) ? int.Parse(id) : num;
        var member = await _context.Members.AsQueryable().AsNoTracking().FirstOrDefaultAsync(d => d.Id == memberId);
        if (member is null)
            return new MemberDetailViewModel();

        var memberDetailViewModel = _mapper.Map<MemberDetailViewModel>(member);
        memberDetailViewModel.Birthday = member.Birthday.ToString("yyyy-MM-dd");

        return memberDetailViewModel;
    }

    public async Task<(bool, string)> MemberAddAsync(MemberDetailViewModel memberDetailViewModel)
    {
        try
        {
            var identityNumberRepeat = await _context.Members.AsQueryable().AsNoTracking().AnyAsync(d => d.IdentityNumber == memberDetailViewModel.IdentityNumber);
            if (identityNumberRepeat)
                return (false, "身份證字號重複");
            var member = _mapper.Map<Member>(memberDetailViewModel);
            member.Birthday = DateTime.Parse(memberDetailViewModel.Birthday);

            _context.Members.Add(member);
            await _context.SaveChangesAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return (true, string.Empty);
    }

    public async Task<(bool, string)> MemberEditAsync(MemberDetailViewModel memberDetailViewModel)
    {
        try
        {
            var identityNumberRepeat = await _context.Members.AsQueryable().AsNoTracking().AnyAsync(d =>
                d.Id != memberDetailViewModel.Id && d.IdentityNumber == memberDetailViewModel.IdentityNumber);
            if (identityNumberRepeat)
                return (false, "身份證字號重複");
            var member = _mapper.Map<Member>(memberDetailViewModel);
            member.Birthday = DateTime.Parse(memberDetailViewModel.Birthday);

            _context.Members.Update(member);
            await _context.SaveChangesAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return (true, string.Empty);
    }

    public async Task<List<ParticipantBox>> GetAllMemberList()
    {
        return await _context.Members.AsQueryable().AsNoTracking()
            .Select(d => new ParticipantBox {MemberId = d.Id, MemberName = d.Name}).ToListAsync();
    }
}