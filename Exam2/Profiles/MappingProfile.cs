﻿using AutoMapper;
using Exam2.DBModel;
using Exam2.Models.ViewModels;

namespace Exam2.Profiles;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<MemberDetailViewModel, Member>().ReverseMap()
            .ForMember(x => x.Birthday, y => y.Ignore());
        CreateMap<Member, MemberDetailViewModel>().ReverseMap()
            .ForMember(x => x.Birthday, y => y.Ignore());
        CreateMap<ActivityList, ActivityDetailViewModel>().ReverseMap()
            .ForMember(x => x.StartDate, y => y.Ignore())
            .ForMember(x => x.EndDate, y => y.Ignore());
        CreateMap<ActivityDetailViewModel, ActivityList>().ReverseMap()
            .ForMember(x => x.StartDate, y => y.Ignore())
            .ForMember(x => x.EndDate, y => y.Ignore());
    }
    
}