﻿using AutoMapper.Execution;
using Exam2.Models.Interfaces;
using Exam2.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Exam2.Controllers;

public class MemberController : Controller
{
    private readonly IMemberService _memberService;

    public MemberController(IMemberService memberService)
    {
        _memberService = memberService;
    }

    public async Task<IActionResult> Index([FromQuery]string nameCondition, [FromQuery]string birthdayYearCondition)
    {
        var memberSearchViewModel = new MemberSearchViewModel
        {
            NameCondition = nameCondition,
            BirthdayYearCondition = birthdayYearCondition
        };
        var memberListQuery = await _memberService.MemberListQuery(memberSearchViewModel);
        
        return View(memberListQuery);
    }

    public async Task<IActionResult> Detail(string id, string actionType)
    {
        var memberQueryById = await _memberService.MemberQueryById(id);
        ViewData["actionType"] = actionType;
        ViewBag.sexList = SexListSetting(memberQueryById.Sex);

        return View(memberQueryById);
    }
    
    private static List<SelectListItem> SexListSetting(string sex)
    {
        return new List<SelectListItem>
        {
            new SelectListItem
            {
                Text = "男",
                Value = "M",
                Selected = sex == "M" || string.IsNullOrWhiteSpace(sex),
            },
            new SelectListItem
            {
                Text = "女",
                Value = "F",
                Selected = sex == "F",
            },
        };
    }

    [HttpPost]
    public async Task<IActionResult> MemberAdd([FromBody]MemberDetailViewModel memberDetailViewModel)
    {
        if (!ModelState.IsValid) return BadRequest(ModelState);
        var result = await _memberService.MemberAddAsync(memberDetailViewModel);
        if (!result.Item1)
        {
            ModelState.AddModelError(nameof(memberDetailViewModel.IdentityNumber), result.Item2);
            return BadRequest(ModelState);
        }
            
        return Json(true);
    }

    [HttpPost]
    public async Task<IActionResult> MemberEdit([FromBody]MemberDetailViewModel memberDetailViewModel)
    {
        if (!ModelState.IsValid) return BadRequest(ModelState);
        var result = await _memberService.MemberEditAsync(memberDetailViewModel);
        if (!result.Item1)
        {
            ModelState.AddModelError(nameof(memberDetailViewModel.IdentityNumber), result.Item2);
            return BadRequest(ModelState);
        }
            
        return Json(true);
    }
}