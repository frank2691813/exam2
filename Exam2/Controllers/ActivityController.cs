﻿using Exam2.Models.Interfaces;
using Exam2.Models.Services;
using Exam2.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Exam2.Controllers;

public class ActivityController : Controller
{
    private readonly IActivityService _activityService;
    private readonly IMemberService _memberService;

    public ActivityController(IActivityService activityService, IMemberService memberService)
    {
        _activityService = activityService;
        _memberService = memberService;
    }

    public async Task<IActionResult> Index([FromQuery]string activityName, [FromQuery]string startDate, [FromQuery]string endDate)
    {
        var activitySearchViewModel = new ActivitySearchViewModel
        {
            ActivityNameCondition = activityName,
            StartDateCondition = startDate,
            EndDateCondition = endDate,
        };

        var activityListQuery = await _activityService.ActivityListQueryAsync(activitySearchViewModel);

        return View(activityListQuery);
    }

    public async Task<IActionResult> Detail(string id, string actionType)
    {
        ViewData["actionType"] = actionType;
        if (actionType == "Add")
            return View(new ActivityDetailViewModel{UnSelectCheckBoxes = await _memberService.GetAllMemberList()});
        var activity = await _activityService.ActivityListQueryByIdAsync(id);

        return View(activity);
    }

    [HttpPost]
    public async Task<IActionResult> ActivityAdd([FromBody]ActivityDetailViewModel activityDetailViewModel)
    {
        if (!ModelState.IsValid && ModelStateInValid(activityDetailViewModel, ModelState))
            return BadRequest(ModelState);
        var result = await _activityService.ActivityAddAsync(activityDetailViewModel);
        return Json(result);
    }

    [HttpPost]
    public async Task<IActionResult> ActivityEdit([FromBody]ActivityDetailViewModel activityDetailViewModel)
    {
        if (!ModelState.IsValid && ModelStateInValid(activityDetailViewModel, ModelState))
            return BadRequest(ModelState);
        var result = await _activityService.ActivityEditAsync(activityDetailViewModel);
        return Json(result);
    }

    private static bool ModelStateInValid(ActivityDetailViewModel activityDetailViewModel, ModelStateDictionary modelStateDictionary)
    {
        var modelStateKeys = new List<string>();
        foreach (var propertyInfo in activityDetailViewModel.GetType().GetProperties())
        {
            if ((propertyInfo.PropertyType == typeof(int) || propertyInfo.PropertyType == typeof(string)) && modelStateDictionary.ContainsKey(propertyInfo.Name))
            {
                return true;
            }
        }

        return false;
    }
}