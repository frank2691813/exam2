﻿using System;
using System.Collections.Generic;

namespace Exam2.DBModel
{
    public partial class ActivityList
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Fee { get; set; }
        public int ParticipantNumber { get; set; }
        public string Place { get; set; } = null!;
    }
}
