﻿using System;
using System.Collections.Generic;

namespace Exam2.DBModel
{
    public partial class ActivityParticipant
    {
        public int Id { get; set; }
        public int ActivityId { get; set; }
        public int MemberId { get; set; }
    }
}
